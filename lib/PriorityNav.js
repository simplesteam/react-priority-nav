'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames2 = require('classnames');

var _classnames3 = _interopRequireDefault(_classnames2);

var _HandleOutsideClick = require('./HandleOutsideClick');

var _HandleOutsideClick2 = _interopRequireDefault(_HandleOutsideClick);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var PriorityNav = function (_Component) {
  _inherits(PriorityNav, _Component);

  function PriorityNav(props) {
    _classCallCheck(this, PriorityNav);

    var _this = _possibleConstructorReturn(this, (PriorityNav.__proto__ || Object.getPrototypeOf(PriorityNav)).call(this, props));

    _this.listItemsElem = {};

    var childUl = _this.props.children[0] || _this.props.children;

    _this.state = {
      priorityItems: _react2.default.Children.map(childUl.props.children, function (child, idx) {
        if (!child) return child;

        return _react2.default.cloneElement(child, {
          'data-ref': idx,
          ref: function ref(_ref) {
            _this.listItemsElem[idx] = _ref;
          },
          style: Object.assign({}, child.props.style || {}, {
            display: 'inline-block'
          })
        });
      }),
      overflownItems: [],
      show: false
    };

    _this.handleNav = _this.handleNav.bind(_this);
    _this.handleOutsideClick = _this.handleOutsideClick.bind(_this);
    return _this;
  }

  _createClass(PriorityNav, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      window.addEventListener('resize', this.handleNav);
      this.handleNav();
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      var _this2 = this;

      this.listItemsElem = {};

      var childUl = nextProps.children[0] || nextProps.children;

      this.setState({
        priorityItems: _react2.default.Children.map(childUl.props.children, function (child, idx) {
          if (!child) return child;

          return _react2.default.cloneElement(child, {
            'data-ref': idx,
            ref: function ref(_ref2) {
              _this2.listItemsElem[idx] = _ref2;
            }
          });
        }),
        overflownItems: [],
        requiresResize: true
      });
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (this.state.requiresResize) {
        this.handleNav();
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.removeEventListener('resize', this.handleNav);
    }
  }, {
    key: 'handleNav',
    value: function handleNav() {
      var _this3 = this;

      if (!this.navWrapper) return;

      var navWrapperWidth = this.navWrapper.offsetWidth;
      var combinedListItems = [].concat(_toConsumableArray(this.state.priorityItems), _toConsumableArray(this.state.overflownItems));

      var result = combinedListItems.map(function (component) {
        var currentWidthOfLink = _this3.listItemsElem[component.props['data-ref']].offsetWidth || 0;
        return _react2.default.cloneElement(component, {
          'data-largestwidth': Math.max(component.props['data-largestwidth'] || 0, currentWidthOfLink)
        });
      }).reduce(function (state, component, idx, links) {
        var lastItem = idx === links.length - 1;
        var widthOfLink = component.props['data-largestwidth'];
        var paddingSpacer = lastItem ? 20 : 90;

        if (!state.reachedLimit && state.widthSoFar + widthOfLink + paddingSpacer < navWrapperWidth) {
          return Object.assign({}, state, {
            priorityItems: [].concat(_toConsumableArray(state.priorityItems), [component]),
            widthSoFar: state.widthSoFar + widthOfLink
          });
        }

        return Object.assign({}, state, {
          overflownItems: [].concat(_toConsumableArray(state.overflownItems), [component]),
          reachedLimit: true
        });
      }, {
        priorityItems: [],
        overflownItems: [],
        widthSoFar: 0,
        reachedLimit: false
      });

      this.setState({
        priorityItems: result.priorityItems,
        overflownItems: result.overflownItems,
        requiresResize: false
      });
    }
  }, {
    key: 'handleOutsideClick',
    value: function handleOutsideClick() {
      this.setState({ show: false });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this4 = this;

      var _state = this.state,
          priorityItems = _state.priorityItems,
          overflownItems = _state.overflownItems,
          show = _state.show;


      return _react2.default.createElement(
        'nav',
        {
          className: this.props.navClass,
          ref: function ref(_ref3) {
            _this4.navWrapper = _ref3;
          }
        },
        _react2.default.Children.map(this.props.children, function (child) {
          return _react2.default.cloneElement(child, {
            children: priorityItems
          });
        }),
        overflownItems.length > 0 && _react2.default.createElement(
          _HandleOutsideClick2.default,
          { onOutsideClick: this.handleOutsideClick },
          _react2.default.createElement(
            'span',
            {
              'aria-haspopup': true,
              className: (0, _classnames3.default)(this.props.dropdownClass, _defineProperty({}, this.props.openClass, show))
            },
            _react2.default.createElement(
              'button',
              {
                'aria-controls': 'menu',
                className: this.props.toggleClass,
                onClick: function onClick() {
                  _this4.setState({
                    show: !_this4.state.show
                  });
                }
              },
              'more'
            ),
            _react2.default.createElement(
              'ul',
              { 'aria-hidden': !show },
              overflownItems
            )
          )
        )
      );
    }
  }]);

  return PriorityNav;
}(_react.Component);

PriorityNav.propTypes = {
  children: _propTypes2.default.node.isRequired,
  navClass: _propTypes2.default.string,
  openClass: _propTypes2.default.string,
  dropdownClass: _propTypes2.default.string,
  toggleClass: _propTypes2.default.string
};

PriorityNav.defaultProps = {
  navClass: 'priority-nav',
  openClass: 'is-open',
  dropdownClass: 'priority-dropdown-wrapper',
  toggleClass: 'priority-toggle'
};

exports.default = {
  PriorityNav: PriorityNav,
  HandleOutsideClick: _HandleOutsideClick2.default
};