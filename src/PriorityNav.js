import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import HandleOutsideClick from './HandleOutsideClick'

class PriorityNav extends Component {
  constructor(props) {
    super(props)

    this.listItemsElem = {}

    const childUl = this.props.children[0] || this.props.children

    this.state = {
      priorityItems: React.Children.map(childUl.props.children, (child, idx) => {
        if (!child) return child

        return React.cloneElement(child, {
          'data-ref': idx,
          ref: (ref) => { this.listItemsElem[idx] = ref },
          style: Object.assign({}, (child.props.style || {}), {
            display: 'inline-block',
          }),
        })
      }),
      overflownItems: [],
      show: false,
    }

    this.handleNav = this.handleNav.bind(this)
    this.handleOutsideClick = this.handleOutsideClick.bind(this)
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleNav)
    this.handleNav()
  }

  componentWillReceiveProps(nextProps) {
    this.listItemsElem = {}

    const childUl = nextProps.children[0] || nextProps.children

    this.setState({
      priorityItems: React.Children.map(childUl.props.children, (child, idx) => {
        if (!child) return child

        return React.cloneElement(child, {
          'data-ref': idx,
          ref: (ref) => { this.listItemsElem[idx] = ref },
        })
      }),
      overflownItems: [],
      requiresResize: true,
    })
  }

  componentDidUpdate() {
    if (this.state.requiresResize) {
      this.handleNav()
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleNav)
  }

  handleNav() {
    if (!this.navWrapper) return

    const navWrapperWidth = this.navWrapper.offsetWidth
    const combinedListItems = [...this.state.priorityItems, ...this.state.overflownItems]

    const result = combinedListItems
      .map((component) => {
        const currentWidthOfLink = this.listItemsElem[component.props['data-ref']].offsetWidth || 0
        return React.cloneElement(component, {
          'data-largestwidth': Math.max(component.props['data-largestwidth'] || 0, currentWidthOfLink),
        })
      })
      .reduce((state, component, idx, links) => {
        const lastItem = idx === links.length - 1
        const widthOfLink = component.props['data-largestwidth']
        const paddingSpacer = (lastItem) ? 20 : 90

        if (
          !state.reachedLimit
          && state.widthSoFar + widthOfLink + paddingSpacer < navWrapperWidth
        ) {
          return Object.assign({}, state, {
            priorityItems: [...state.priorityItems, component],
            widthSoFar: state.widthSoFar + widthOfLink,
          })
        }

        return Object.assign({}, state, {
          overflownItems: [...state.overflownItems, component],
          reachedLimit: true,
        })
      }, {
        priorityItems: [],
        overflownItems: [],
        widthSoFar: 0,
        reachedLimit: false,
      })

    this.setState({
      priorityItems: result.priorityItems,
      overflownItems: result.overflownItems,
      requiresResize: false,
    })
  }

  handleOutsideClick() {
    this.setState({ show: false })
  }

  render() {
    const { priorityItems, overflownItems, show } = this.state

    return (
      <nav
        className={this.props.navClass}
        ref={(ref) => { this.navWrapper = ref }}
      >
        {
          React.Children.map(
            this.props.children,
            child => React.cloneElement(child, {
              children: priorityItems,
            }),
          )
        }

        {
          (overflownItems.length > 0) && (
            <HandleOutsideClick onOutsideClick={this.handleOutsideClick}>
              <span
                aria-haspopup
                className={classnames(this.props.dropdownClass, {
                  [this.props.openClass]: show,
                })}
              >
                <button
                  aria-controls="menu"
                  className={this.props.toggleClass}
                  onClick={() => {
                    this.setState({
                      show: !this.state.show,
                    })
                  }}
                >
                  more
                </button>

                <ul aria-hidden={!show}>
                  { overflownItems }
                </ul>
              </span>
            </HandleOutsideClick>
          )
        }
      </nav>
    )
  }
}

PriorityNav.propTypes = {
  children: PropTypes.node.isRequired,
  navClass: PropTypes.string,
  openClass: PropTypes.string,
  dropdownClass: PropTypes.string,
  toggleClass: PropTypes.string,
}

PriorityNav.defaultProps = {
  navClass: 'priority-nav',
  openClass: 'is-open',
  dropdownClass: 'priority-dropdown-wrapper',
  toggleClass: 'priority-toggle',
}

export default {
  PriorityNav,
  HandleOutsideClick,
}
