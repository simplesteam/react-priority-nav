import React, { Component } from 'react'
import PropTypes from 'prop-types'

class HandleOutsideClick extends Component {
  constructor(props) {
    super(props)

    this.onOutsideClick = this.onOutsideClick.bind(this)
  }

  componentDidMount() {
    document.addEventListener('click', this.onOutsideClick)

    if ('ontouchstart' in document.documentElement) {
      document.body.style.cursor = 'pointer'
    }
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.onOutsideClick)
  }

  onOutsideClick(e) {
    const insideClick = this.wrapperElem.contains(e.target)
    if (!insideClick && this.props.onOutsideClick) this.props.onOutsideClick(e)
  }

  render() {
    return (
      <span ref={(elem) => { this.wrapperElem = elem }}>
        {this.props.children}
      </span>
    )
  }
}

HandleOutsideClick.propTypes = {
  children: PropTypes.node.isRequired,
  onOutsideClick: PropTypes.func,
}

HandleOutsideClick.defaultProps = {
  onOutsideClick: () => {},
}

export default HandleOutsideClick
