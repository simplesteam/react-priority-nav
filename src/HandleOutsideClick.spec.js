import React from 'react'
import { shallow, mount } from 'enzyme'
import should from 'should'
import sinon from 'sinon'

import HandleOutsideClick from './HandleOutsideClick'

describe('<HandleOutsideClick />', () => {
  describe('initial render', () => {
    it('should render', () => {
      const node = <p>HandleOutsideClick component</p>

      const wrapper = shallow(
        <HandleOutsideClick>{node}</HandleOutsideClick>
      )

      wrapper.length.should.eql(1)
      wrapper.find('span').length.should.eql(1) // checking that default wrapper is rendered
      wrapper.children().length.should.be.above(0)
      wrapper.contains(node).should.be.true // renders the child
    })

    it('should call componentDidMount', () => {
      sinon.spy(HandleOutsideClick.prototype, 'componentDidMount')
      mount(<HandleOutsideClick>HandleOutsideClick component</HandleOutsideClick>)
      HandleOutsideClick.prototype.componentDidMount.calledOnce.should.be.true()
    })
  })

  describe('onOutsideClick', () => {
    it('should set onOutsideClick as a prop', () => {
      const node = <p>HandleOutsideClick component</p>

      const wrapper = mount(
        <HandleOutsideClick
          onOutsideClick={() => {}}
        >
          {node}
        </HandleOutsideClick>
      )

      wrapper.props().onOutsideClick.should.be.true
    })

    it('should not trigger onOutsideClick when clicked inside the component', () => {
      const onOutsideClick = sinon.spy()
      const node = <p>HandleOutsideClick component</p>

      const wrapper = mount(
        <HandleOutsideClick
          onOutsideClick={onOutsideClick}
        >
          {node}
        </HandleOutsideClick>
      )

      const insideClick = wrapper.find('p')
      insideClick.simulate('click')

      onOutsideClick.calledOnce.should.be.false
      onOutsideClick.callCount.should.eql(0)
    })

    it('should trigger onOutsideClick when clicked outside of the component', () => {
      const onOutsideClick = sinon.spy()
      const node = <p>HandleOutsideClick component</p>

      const wrapper = mount(
        <HandleOutsideClick
          onOutsideClick={onOutsideClick}
        >
          {node}
        </HandleOutsideClick>
      )

      document.body.click()
      onOutsideClick.calledOnce.should.be.true
      onOutsideClick.callCount.should.eql(1)
    })
  })

  describe('unmount', () => {
    it('should call componentWillUnmount', () => {
      sinon.spy(HandleOutsideClick.prototype, 'componentWillUnmount')
      const wrapper = mount(<HandleOutsideClick>HandleOutsideClick component</HandleOutsideClick>)
      wrapper.unmount()
      HandleOutsideClick.prototype.componentWillUnmount.calledOnce.should.be.true()
    })
  })
})
