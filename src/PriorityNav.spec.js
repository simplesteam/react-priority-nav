import React from 'react'
import { shallow, mount } from 'enzyme'
import should from 'should'
import sinon from 'sinon'

import { PriorityNav } from '../src/index'

describe('<PriorityNav />', () => {
  describe('initial render', () => {
    it('should render each child node in an <li /> tag', () => {
      const wrapper = shallow(
        <PriorityNav>
          <ul>
            <li>
              <a href="#link1">Link 1</a>
            </li>
            <li>
              <a href="#link2">Link 2</a>
            </li>
            <li>
              <a href="#link3">Link 3</a>
            </li>
            <li>
              <a href="#link4">Link 4</a>
            </li>
            <li>
              <a href="#link5">Link 5</a>
            </li>
            <li>
              <a href="#link6">Link 6</a>
            </li>
          </ul>
        </PriorityNav>
      )

      wrapper.length.should.eql(1)
      wrapper.find('nav').length.should.eql(1)
      wrapper.find('ul').length.should.eql(1)
      wrapper.find('li').length.should.eql(6)
      wrapper.find('a').length.should.eql(6)
    })
  })

  describe('props', () => {
    it('should render with default props and let us set new props', () => {
      const wrapper = mount(
        <PriorityNav>
          <ul>
            <li>
              <a href="#link1">Link 1</a>
            </li>
            <li>
              <a href="#link2">Link 2</a>
            </li>
            <li>
              <a href="#link3">Link 3</a>
            </li>
            <li>
              <a href="#link4">Link 4</a>
            </li>
            <li>
              <a href="#link5">Link 5</a>
            </li>
            <li>
              <a href="#link6">Link 6</a>
            </li>
          </ul>
        </PriorityNav>
      )

      wrapper.prop('navClass').should.equal('priority-nav') // check default props
      wrapper.prop('openClass').should.equal('is-open')
      wrapper.prop('dropdownClass').should.equal('priority-dropdown-wrapper')
      wrapper.prop('toggleClass').should.equal('priority-toggle')

      wrapper.setProps({
        navClass: "new-priority-nav-class",
        openClass: "new-is-open-class",
        dropdownClass: "new-priority-dropdown-wrapper-class"
      })

      wrapper.prop('navClass').should.equal('new-priority-nav-class') // check new props have been set
      wrapper.prop('openClass').should.equal('new-is-open-class')
      wrapper.prop('dropdownClass').should.equal('new-priority-dropdown-wrapper-class')
      wrapper.prop('toggleClass').should.equal('priority-toggle')
      wrapper.prop('toggleClass').should.not.equal('new-priority-toggle-class')
    })
  })

  describe('refs', () => {
    it('should have \'navWrapper\' ref on PriorityNav', () => {
      const wrapper = mount(
        <PriorityNav>
          <ul>
            <li>
              <a href="#link1">Link 1</a>
            </li>
            <li>
              <a href="#link2">Link 2</a>
            </li>
            <li>
              <a href="#link3">Link 3</a>
            </li>
            <li>
              <a href="#link4">Link 4</a>
            </li>
            <li>
              <a href="#link5">Link 5</a>
            </li>
            <li>
              <a href="#link6">Link 6</a>
            </li>
          </ul>
        </PriorityNav>
      )

      should.exist(wrapper.instance().navWrapper)
    })

    it('should have \'data-ref\' & \'data-largestwidth\' assigned to each child node', () => {
      const wrapper = mount(
        <PriorityNav>
          <ul>
            <li>
              <a href="#link1">Link 1</a>
            </li>
            <li>
              <a href="#link2">Link 2</a>
            </li>
            <li>
              <a href="#link3">Link 3</a>
            </li>
            <li>
              <a href="#link4">Link 4</a>
            </li>
            <li>
              <a href="#link5">Link 5</a>
            </li>
            <li>
              <a href="#link6">Link 6</a>
            </li>
          </ul>
        </PriorityNav>
      )

      wrapper.find('li').forEach((node) => {
        node.prop('data-ref').should.be.true
        node.prop('data-largestwidth').should.be.true
        node.prop('data-largestwidth').should.eql(0) // checking default width
      })
    })
  })

  describe('handleNav', () => {
    it('should call handleNav', () => {
      const wrapper = mount(
        <PriorityNav>
          <ul>
            <li>
              <a href="#link1">Link 1</a>
            </li>
            <li>
              <a href="#link2">Link 2</a>
            </li>
            <li>
              <a href="#link3">Link 3</a>
            </li>
            <li>
              <a href="#link4">Link 4</a>
            </li>
            <li>
              <a href="#link5">Link 5</a>
            </li>
            <li>
              <a href="#link6">Link 6</a>
            </li>
          </ul>
        </PriorityNav>
      )

      const onHandleNav = sinon.spy(wrapper.instance(), 'handleNav')
      wrapper.update()
      onHandleNav.called.should.be.true()
    })

    describe('overflown items', () => {
      it('should not move items into \'overflownItems\' when the offsetWidth of PriorityNav is more than \'combinedListItemsWidth\'', () => {
        const wrapper = mount(
          <PriorityNav>
            <ul>
              <li data-largestwidth={100}>
                <a href="#link1">Link 1</a>
              </li>
              <li data-largestwidth={100}>
                <a href="#link2">Link 2</a>
              </li>
              <li data-largestwidth={100}>
                <a href="#link3">Link 3</a>
              </li>
              <li data-largestwidth={100}>
                <a href="#link4">Link 4</a>
              </li>
              <li data-largestwidth={100}>
                <a href="#link5">Link 5</a>
              </li>
              <li data-largestwidth={100}>
                <a href="#link6">Link 6</a>
              </li>
            </ul>
          </PriorityNav>
        )

        const instance = wrapper.instance()

        instance.navWrapper.offsetWidth = 1262
        instance.handleNav()

        let combinedListItemsWidth = 0

        wrapper.find('li').forEach((node) => {
          combinedListItemsWidth += parseInt(node.prop('data-largestwidth'), 10)
        })

        combinedListItemsWidth.should.be.lessThan(instance.navWrapper.offsetWidth)

        const actualList = wrapper.find('li').length
        const priorityItems = wrapper.state('priorityItems').length

        actualList.should.eql(priorityItems)
        priorityItems.should.eql(6)
      })

      it('should move items into \'overflownItems\' when the offsetWidth of PriorityNav is less than \'combinedListItemsWidth\'', () => {
        const wrapper = mount(
          <PriorityNav>
            <ul>
              <li data-largestwidth={100}>
                <a href="#link1">Link 1</a>
              </li>
              <li data-largestwidth={100}>
                <a href="#link2">Link 2</a>
              </li>
              <li data-largestwidth={100}>
                <a href="#link3">Link 3</a>
              </li>
              <li data-largestwidth={100}>
                <a href="#link4">Link 4</a>
              </li>
              <li data-largestwidth={100}>
                <a href="#link5">Link 5</a>
              </li>
              <li data-largestwidth={100}>
                <a href="#link6">Link 6</a>
              </li>
            </ul>
          </PriorityNav>
        )

        const instance = wrapper.instance()

        instance.navWrapper.offsetWidth = 368
        instance.handleNav()

        let combinedListItemsWidth = 0

        wrapper.find('li').forEach((node) => {
          combinedListItemsWidth += parseInt(node.prop('data-largestwidth'), 10)
        })

        combinedListItemsWidth.should.be.greaterThan(instance.navWrapper.offsetWidth)

        const priorityItems = wrapper.state('priorityItems').length // 2 items in priorityItems
        const overflownItems = wrapper.state('overflownItems').length // 4 items in overflownItems

        priorityItems.should.eql(2)
        overflownItems.should.eql(4)

        priorityItems.should.not.eql(overflownItems)
      })

      it('should show the \'overflownItems\' when clicked on \'more\' button', () => {
        const wrapper = mount(
          <PriorityNav>
            <ul>
              <li data-largestwidth={100}>
                <a href="#link1">Link 1</a>
              </li>
              <li data-largestwidth={100}>
                <a href="#link2">Link 2</a>
              </li>
              <li data-largestwidth={100}>
                <a href="#link3">Link 3</a>
              </li>
              <li data-largestwidth={100}>
                <a href="#link4">Link 4</a>
              </li>
              <li data-largestwidth={100}>
                <a href="#link5">Link 5</a>
              </li>
              <li data-largestwidth={100}>
                <a href="#link6">Link 6</a>
              </li>
            </ul>
          </PriorityNav>
        )

        const instance = wrapper.instance()

        instance.navWrapper.offsetWidth = 368
        instance.handleNav()

        const dropdown = wrapper.find('.priority-dropdown-wrapper')
        const ariaHidden = dropdown.find('ul').prop('aria-hidden')
        const button = wrapper.find('button')

        ariaHidden.should.be.true
        dropdown.hasClass('is-open').should.be.false

        button.simulate('click')

        dropdown.hasClass('is-open').should.be.true
        ariaHidden.should.be.false
      })
    })
  })
})
